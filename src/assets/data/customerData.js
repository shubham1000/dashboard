const customer_data =[
  {
    id:1,
    customer:"Cust1",
    service:"App",
    paid:"yes",
  },
  {
    id:2,
    customer:"Cust2",
    service:"AI",
    paid:"yes",
  },
  {
    id:3,
    customer:"Cust3",
    service:"React",
    paid:"No",
  }
] 

export default customer_data;
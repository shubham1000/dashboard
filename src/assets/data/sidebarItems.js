import home from '../../assets/svgs/home.svg';
import about from '../../assets/svgs/about.svg';
import settings from '../../assets/svgs/settings.svg';

 const sidebar_Items =[
  {
    id:1,
    title:"Home",
    path:"/",
    icon:home,
  },
  {
    id:2,
    title:"About",
    path:"/about",
    icon:about
  },
  {
    id:3,
    title:"Setting",
    path:"/setting",
    icon:settings
  }
] 

export default sidebar_Items
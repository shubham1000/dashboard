import {observable} from "mobx"; 

export var navbarMenu=observable({
  navbarMenuClicked:false,
  navbarMenuValue:function(){
    return this.navbarMenuClicked;
  }
});

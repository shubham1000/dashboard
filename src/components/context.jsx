import React from 'react'

const CustomerContext =React.createContext()

const CustomerProvider = CustomerContext.Provider
const CustomerConsumer=CustomerContext.Consumer

export {CustomerConsumer,CustomerProvider}
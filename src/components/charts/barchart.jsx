import React from 'react'
import { Bar } from 'react-chartjs-2';
import '../../css/layout/chart.css';




  const BarChart=()=> {
    return (
      <div className="charts">
        <Bar className="chart" 
        data={{
          labels:['Jan','Feb','Mar',],
          datasets:[{
            label:"No. of customer",
            data:[10,30,15], 
            backgroundColor:['rgba(255, 99, 132, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',]
          }
          ]
        }}
        options={{maintainAspectRatio:true,
        responsive:false}}
        
        />
      </div>
    )
  }

  export default BarChart;
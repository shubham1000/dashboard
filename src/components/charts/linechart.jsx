import React from 'react'
import { Line } from "react-chartjs-2";
import '../../css/layout/chart.css';




  const LineChart=()=> {
    return (
      <div className="charts">
        <Line className="chart" 
        data={{
          labels:['Jan','Feb','Mar',],
          datasets:[{
            label:"Sales pre month ",
            data:[100,200,150], 
            backgroundColor:['rgba(255, 99, 132, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',]
          }
          ]
        }}
        options={{maintainAspectRatio:true,
        responsive:false}}
        
        />
      </div>
    )
  }

  export default LineChart;
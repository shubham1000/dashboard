import React,{useContext,useState} from 'react'
import '../../css/layout/table.css';
import { observer} from "mobx-react-lite";
import customer_data from "../../assets/data/customerData";



const CustomerTable2 = observer(()=>{

  

  return (
    <table className="customer-table">
     <tr>
       <th>id</th>
       <th>Customer</th>
       <th>Service</th>
       <th>Paid</th>
     </tr>
     {
       customer_data.forEach((customer,index)=>{
        <tr>
        <td>{customer.id}</td>
        <td>{customer.customer}</td>
        <td>{customer.service}</td>
        <td>{customer.paid}</td>
      </tr>
       })
     }
     
    </table>
    
  );
})

export default CustomerTable2;
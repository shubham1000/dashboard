import React,{useState} from 'react'
import {BrowserRouter as Router,Routes,Route,useLocation} from 'react-router-dom'
import Navbar from './navbar'
import Sidebar from './sidebar'
import Home from './home'
import About from './about'
import Setting from './setting'
import '../../css/layout/layout.css';
import { observer} from "mobx-react-lite";
import {navbarMenu} from '../../observable/navbarMenuObservable';
import customer_data from '../../assets/data/customerData';

export const CustomerDataContext=React.createContext();
//layout provide basic placeholder for the website components 
const Layout = observer(() =>{
  
  const [customerData, setcustomerData] = useState(customer_data)
  const [addCustomer, setaddCustomer] = useState(
    { 
     customer:"",
     service:""
   }
   )
  const [editData, setEditData]=useState({id:"",customer:"",service:"",paid:""})

  const getCustomer=(id)=>{
      const customer=customerData.find(i=>i.id===id);
      return customer;
    }
  const onEdit=(id)=>{
    const oldCustomer=customerData;
    const index = oldCustomer.indexOf(getCustomer(id));
    const selectCustomer=oldCustomer[index]
    console.log(selectCustomer);
  }

  const onDelete=(id)=>{
    const deleteCustomer=customerData.filter(i=>i.id!==id);
    setcustomerData(deleteCustomer);
  }

  const updateValue=(e)=>{
    const name= e.target.name;
    const value =e.target.value;
    setaddCustomer({...addCustomer,[name]:value})
}
const handleSubmit=(e)=>{
  e.preventDefault();
  console.log("submited data ");
  const oldData=customerData;
  const index=customerData.length+1;
  const newCustomer={...addCustomer,id:customerData.length+1,paid:"Yes"}
  console.log(newCustomer)
  const newData={...oldData,newCustomer}
  console.log(newData)

}

  return (

    <CustomerDataContext.Provider value={{customer:customerData,deleteCustomer:onDelete,addCustomers:handleSubmit,updateCustomer:updateValue}}> 
    <Router>
      <Navbar/>
      <Sidebar />
      <div className={`container ${navbarMenu.navbarMenuClicked?"collapse":""}`}>
          <Routes>
            <Route exact path="/" element={<Home/>} />
            <Route path="/about" element={<About/>}/>
            <Route path='/setting' element={<Setting/>}/>
            <Route path="*" element={<NoMatch/>}/>
          </Routes>
      </div>
      
    </Router>
    </CustomerDataContext.Provider>

  )
})

export default Layout

const NoMatch=()=> {
  let location = useLocation();

  return (
    <div>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}

import React,{useState,useContext} from 'react'
import '../../css/layout/form.css';
import { CustomerDataContext } from './layout';
import customer_data from '../../assets/data/customerData';



const CustomerForm = ()=>{
  const {customer,addCustomers,updateCustomer} = useContext(CustomerDataContext)

  
  
  
  
  
  return (
    <div>
      <form action="" onSubmit={addCustomers}>
        <div className="forms">
        <label htmlFor="customer" className="form-text">Customer Name</label>
        <input type="text" 
        onChange={updateCustomer}
        name="customer" 
        id="customer" className="form-input" 
        autoComplete="off"/>  
        </div>
        
        <div className="forms">
        <label htmlFor="service" className="form-text">Service</label>
        <input type="text" 
        onChange={updateCustomer}
        name="service" 
        id="service" className="form-input" 
        autoComplete="off"/>
        </div>
        <button type="submit" className="form-button">Add Customer</button>        
      </form>
    </div>
  );
}

export default CustomerForm;
import { runInAction } from 'mobx';
import React from 'react';
import {Link} from 'react-router-dom';
import man from '../../assets/images/man.png'
import notification from '../../assets/images/notification.png'
import menu from '../../assets/svgs/menu.svg';
import '../../css/layout/navbar.css';
import {navbarMenu} from '../../observable/navbarMenuObservable';


// const imgUrl =process.env.PUBLIC_URL;


function menuClicked(){
  runInAction(()=>{navbarMenu.navbarMenuClicked= !navbarMenu.navbarMenuClicked;});
  
  console.log("navbar " +navbarMenu.navbarMenuClicked);

}
function Navbar() {

  return (
    <>
      <nav className="navbar">
            <div className="menu-icon" onClick={menuClicked}>
              <img src={menu} alt="menu" />
            </div>
            <Link to="/" className="navbar-logo">
              Bruviti 
                {/* <img src={process.env.PUBLIC_URL + 'bruviti2.png'} alt="Bruviti" className="companyLogo"/> */}
            </Link>
           
            <ul className="profile-icons">
              <li><img src={notification} alt="notification" className="profile-icon"/></li>
              <li><img src={man} alt="man" className="profile-icon" /></li>
            </ul>
      </nav>
    </>
  )
}

export default Navbar

import React,{useContext,useState} from 'react'
import '../../css/layout/table.css';
import TableRow from '../elements/tableRow';
import { CustomerDataContext } from './layout';
import edit from '../../assets/svgs/edit.svg';
import deletes from '../../assets/svgs/deletes.svg';



const CustomerTable = ()=>{

  const {customer,deleteCustomer} = useContext(CustomerDataContext)
  
  // const [customers, setcustomers] = useState(customer_data)

  return (
    <table className="customer-table">
      <tbody>

     <tr>
       <th>id</th>
       <th>Customer</th>
       <th>Service</th>
       <th>Paid</th>
       <th>Delete</th>
     </tr>
     {/* {
     customerData.map((customers,index)=>{
        <TableRow id={customers.id} customer={customers.customer} service={customers.service}
       paid={customers.paid}/>
     })  
     } */}
     {
     customer.map((customers,index)=>{
       return(
    <tr>
      <td>{customers.id}</td>
      <td>{customers.customer}</td>
      <td>{customers.service}</td>
      <td>{customers.paid}</td>
      <td> <img src={deletes} alt="delete" onClick={()=>{deleteCustomer(customers.id)}} /></td>
    </tr>
       )
      
     })  
     }
     
    
     
     {/* <tr>
       <td>1</td>
       <td>Cust1</td>
       <td>App</td>
       <td>yes</td>
     </tr>
     <tr>
       <td>2</td>
       <td>Cust2</td>
       <td>AI</td>
       <td>yes</td>
     </tr>
     <tr>
       <td>3</td>
       <td>Cus31</td>
       <td>React</td>
       <td>No</td>
     </tr> */}
      </tbody>

    </table>
  );
}

export default CustomerTable;
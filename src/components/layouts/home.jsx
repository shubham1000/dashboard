import React from "react"
import '../../css/layout/home.css';
import BarCharts from "../charts/barchart";
import PieChart from "../charts/piechart";
import LineChart from "../charts/linechart";
import CustomerForm from "./form";
import CustomerTable from "./table";
// import CustomerTable2 from "./table2";
const Home=()=>{
return <>
<div className="rows">
<div className="card-row">
  <div className="card"><BarCharts/></div>
  <div className="card"><PieChart/></div>
  <div className="card"><LineChart/></div>
</div>
<div className="card-row">
  <div className="card"><CustomerForm/></div>
  <div className="card"><CustomerTable/></div>
</div>
</div>

</>
}

export default Home

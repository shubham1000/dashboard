import {React} from "react"
import '../../css/layout/sidebar.css';
import {navbarMenu} from '../../observable/navbarMenuObservable';
import { observer} from "mobx-react-lite";
import sidebar_Items from "../../assets/data/sidebarItems";
import SideBarButton from "../elements/sidebarButton";



const Sidebar =observer(()=>{

  return (
  <div className={`sidebar ${navbarMenu.navbarMenuClicked?"collapse":""}`} >
    <ul>
      {sidebar_Items.map((menuItem,index)=>(
      <SideBarButton key={index} icon={menuItem.icon} 
      name={menuItem.title} 
      path={menuItem.path}
       />))}
      {/* <li >
        
        <a className="menu-link" >
        <img src={home} className="menu-icons"/>
        <span>Home</span>
        </a>
      </li>
      <li >
        
        <a className="menu-link">
        <img src={about} className="menu-icons"/>
        <span>About</span>
        </a>
      </li>
      <li >
        
        <a className="menu-link">
        <img src={settings} className="menu-icons"/>
        <span>Setting</span>
        </a>
      </li> */}
    </ul>
  </div>
  );
});

export default Sidebar
import React from 'react'

function TableRow(props) {
  return (
    <>
  <tr>
    <td>{props.id}</td>
    <td>{props.customer}</td>
    <td>{props.service}</td>
    <td>{props.paid}</td>
  </tr>
  </>
    
  )
}

export default TableRow

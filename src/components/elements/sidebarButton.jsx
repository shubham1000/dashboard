import React from 'react'
import { Link } from 'react-router-dom';
import '../../css/elements/sidebarButton.css';

function SideBarButton(props) {
  return (
    
       <li key={props.key}>
        <Link className="menu-link" to={props.path} >
        <img src={props.icon} className="menu-icons" alt=""/>
        <span>{props.name}</span>
        </Link>
      </li>
    
  )
}

export default SideBarButton
